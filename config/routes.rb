Rails.application.routes.draw do

  devise_for :users
  root to: 'home#index'
  get '/portal', to: 'portal#index'
  get '/portal', to: 'portal#get_index_table'
  get '/room/:node', to: 'portal#room'
  get '/test', to: 'test#index'
  #get '/reports', to: 'reports#reports'
  get '/reports', to: 'reports#reports'
  post '/data/:node/:id/:data/update', to: 'data#update'
  post '/data/:id/:data/create', to: 'data#create'

end
