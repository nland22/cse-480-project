# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

puts 'SETTING UP DEFAULT USER LOGIN'
user = User.create! :first_name => 'First_User_First', :last_name => 'First_User_last',  :email => 'user@example.com', :password => 'password', :password_confirmation => 'password'
puts 'New user created: ' << user.first_name
user2 = User.create! :first_name => 'Second_User_First', :last_name => 'Second_User_Last', :email => 'user2@example.com', :password => 'password', :password_confirmation => 'password'
puts 'New user created: ' << user2.first_name