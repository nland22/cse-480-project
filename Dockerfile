FROM ruby:2.3.0
RUN apt-get update -qq && apt-get install -y build-essential nodejs
RUN mkdir /myapp

WORKDIR /tmp
COPY Gemfile Gemfile
COPY Gemfile.lock Gemfile.lock
RUN bundle install

ADD . /myapp
WORKDIR /myapp
ENV RAILS_ENV=production
CMD ["rails", "server","-b", "0.0.0.0"]
