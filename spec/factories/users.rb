
# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do
    first_name 'Second_User_First'
    last_name 'Second_User_last'
    email 'user2@example.com'
    password 'password'
    password_confirmation 'password'
    # required if the Devise Confirmable module is used
    #confirmed_at Time.now
  end
end