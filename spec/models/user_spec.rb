require 'rails_helper'
require 'user'

describe User do

  before(:each) do
    @attr = {
      :first_name => "Example First",
      :last_name  => "Example Last",
      :email => "user@example.com",
      :password => "password",
      :password_confirmation => "password"
    }
  end

  it "expect to create a new instance given valid attributes" do
    user = User.create!(@attr)
    expect(user.first_name).to eql("Example First")
    expect(user.last_name).to eql("Example Last")
    expect(user.email).to eql("user@example.com")
    expect(user.password).to eql("password")
    expect(user.password_confirmation).to eql("password")
  end

  it "expected to require an email address" do
    no_email_user = User.new(@attr.merge(:email => ""))
    expect(no_email_user).not_to be_valid
  end

  it "expected to accept valid email addresses" do
    addresses = %w[user@foo.com THE_USER@foo.bar.org first.last@foo.jp]
    addresses.each do |address|
      valid_email_user = User.new(@attr.merge(:email => address))
      expect(valid_email_user).to be_valid
    end
  end

  it "expected to reject invalid email addresses" do
    addresses = %w[user@foo,com user_at_foo.org example.user@foo.]
    addresses.each do |address|
      invalid_email_user = User.new(@attr.merge(:email => address))
      expect(invalid_email_user).not_to be_valid
    end
  end

  it "expected to reject duplicate email addresses" do
    User.create!(@attr)
    user_with_duplicate_email = User.new(@attr)
    expect(user_with_duplicate_email).not_to be_valid
  end

  it "expected to reject email addresses identical up to case" do
    upcased_email = @attr[:email].upcase
    User.create!(@attr.merge(:email => upcased_email))
    user_with_duplicate_email = User.new(@attr)
    expect(user_with_duplicate_email).not_to be_valid
  end

  describe "passwords" do

    before(:each) do
      @user = User.new(@attr)
    end

    it "expected to have a password attribute" do
      expect(@user).to respond_to(:password)
    end

    it "expected to have a password confirmation attribute" do
      expect(@user).to respond_to(:password_confirmation)
    end
  end

  describe "password validations" do

    it "expected to require a password" do
      expect(User.new(@attr.merge(:password => "", :password_confirmation => "")))
      .not_to be_valid
    end

    it "expected to require a matching password confirmation" do
      expect(User.new(@attr.merge(:password_confirmation => "invalid")))
      .not_to be_valid
    end

    it "expect to reject short passwords" do
      short = "a" * 5
      hash = @attr.merge(:password => short, :password_confirmation => short)
      expect(User.new(hash)).not_to be_valid
    end

  end

  describe "password encryption" do

    before(:each) do
      @user = User.create!(@attr)
    end

    it "expect to have an encrypted password attribute" do
      expect(@user).to respond_to(:encrypted_password)
    end

    it "expect to set the encrypted password attribute" do
      expect(@user.encrypted_password).not_to be_blank
    end

  end

end



# RSpec.describe User, type: :model do
    
#     let(:user) {User.new(first_name: 'first', 
#                          last_name: 'last', 
#                          email: 'name@mail.com', 
#                          encrypted_password: 'password')}
#     # subject {User.new(first_name: '', 
#     #                       last_name: '', 
#     #                       email: '', 
#     #                       encrypted_password: '')}

#     context 'user expect to have name email password' do
#         # user = User.new(first_name: 'first', 
#         #                 last_name: 'last', 
#         #                 email: 'name@mail.com', 
#         #                 encrypted_password: 'password')
#         it {expect(user).to have_attributes(:first_name => 'first', 
#                                             :last_name => 'last', 
#                                             :email => 'name@mail.com', 
#                                             :encrypted_password => 'password')}
#     end
    
#     describe 'expect to not be accepted as blank' do
#         # user2 = User.new(first_name: '', 
#         #                 last_name: '', 
#         #                 email: '', 
#         #                 encrypted_password: '')
#         it {expect(subject.first_name).to be_empty}
#         it {expect(subject.last_name).to be_empty}
#         it {expect(subject.email).to be_empty}
#         it {expect(subject.encrypted_password).to be_empty}
#     end
    
# end