# Docker stuff

-   Make sure that [Docker](https://docs.docker.com/) is installed on your
system

-   You will probably also need [Virtualbox](https://www.virtualbox.org/wiki/Downloads)

-   You'll also likely need [Docker Machine](https://docs.docker.com/machine/)

Once you have met those requirements, go into the root of the project (where
  the Gemfile is) and run the following command

```sh
docker build -t effeval .
```

`effeval` is the name of the container that contains the application. Then
you'll want to run the following

```sh
docker run --name effeval -p 8080:3000 -d effeval
```

That should be it. You'll need to know the IP address of the docker machine
that you're running, and you can do that by doing `docker-machine ip
default`. In this case `default` is the name of the docker vm that you're
running. If you don't know what the name is, run `docker-machine ls` and that
will show you the machines that are currently running.

To be honest, if you run `docker-machine ls` it'll also tell you the IP
address of the machine there too.

I know this seems kind of like a lot to do, but trust me, in the long run
you'll appreciate that you did this :)

Any other questions, just let me know.

## Update

If you have [Docker Compose](https://docs.docker.com/compose/) you can now
just run `docker-compose up` at the root of the project and it should do the
rest of the work for you.

You can also do something like the following

```sh
docker run  --name effeval -v "$(pwd)":/usr/src/app --link mongodb:mongo -p
8080:3000  -d effeval
```

If you run the previous command, you will be able to live edit the
application and see the changes update real time. This is dependent on
the fact that you have a mongo container running named 'mongodb'.

If you need to get the mongo container running, run the following:
`docker run --name mongodb -d mongo`.

__Note__: You will need to update the IP Address in the
`config/mongoid.yml` file to the mongo containers IP Address. You
can run `docker inspect mongodb` to see the IP Address of the container.

### Jen

Take a look at this [rspec test file](https://github.com/ndland/happiness_kpi/blob/master/spec/controllers/emotions_controller_spec.rb).
This should give you an idea about automated testing with [rspec](http://rspec.info/).

### Mongo create collections and sample data inserts
db.network.drop()
db.createCollection( "network" ,
						[ { _id:           { $type: "string" }},
						  { node_id:       { $type: "string" }},
						  { network_id:    { $type: "string" }},
						  { date_created:  { $type: "timestamp" }},
						  { last_modified: { $type: "timestamp" }},
						]      
					)

db.network.insert(
				[
				   { node_id:    "nodeid1", network_id: "networkid1", date_created: Timestamp() }
				 , { node_id:    "nodeid2", network_id: "networkid2", date_created: Timestamp() }
				 , { node_id:    "nodeid3", network_id: "networkid3", date_created: Timestamp() }
				 , { node_id:    "nodeid4", network_id: "networkid4", date_created: Timestamp() }
				 ])




db.createCollection( "node" ,
						[ { _id:           { $type: "string" }},
						  { network_id:    { $type: "string" }},
						  { node_number:   { $type: "string" }},
						  { node_name:     { $type: "string" }},
						  { temperature:   { $type: "double" }},
						  { humidity:      { $type: "double" }},
						  { light:         { $type: "double" }},
						  { lastModified:  { $type: "timestamp" }},
						] )

db.node.insert(
			[
				{  network_id: "networkid1"
            	, node_number: "nodenumber1"
            	, node_name:   "nodename1"
            	, temperature: "75.1"
            	, humidity:    "50.0"
            	, light: "882"
            	, lastModified: Timestamp()
            	}
            ,   {  network_id: "networkid2"
            	, node_number: "nodenumber2"
            	, node_name:   "nodename2"
            	, temperature: "76.2"
            	, humidity:    "52.2"
            	, light: "782"
            	, lastModified: Timestamp()
            	}
            ,   {  network_id: "networkid3"
            	, node_number: "nodenumber3"
            	, node_name:   "nodename3"
            	, temperature: "62.5"
            	, humidity:    "44.1"
            	, light: "682"
            	, lastModified: Timestamp()
            	}
            ,   {  network_id:  "networkid4"
            	, node_number: "nodenumber4"
            	, node_name:   "nodename4"
            	, temperature: "67.2"
            	, humidity:    "84.1"
            	, light: "921"
            	, lastModified: Timestamp()
            	}
            ])
