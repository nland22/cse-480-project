class DataController < ApplicationController

  skip_before_action :verify_authenticity_token
  :null_session

  before_action :authenticate_manual

  def update
    @email = params[:id]
    @data = params[:data]
    @node = params[:node]
    render :html => SensorData.new.write_sensor_data_to_user(@data, @email, @node)
  end

  def create
    @data = params[:data]
    # This prints the data as we expect it.
    puts @data
    # This will show the data, but it's not decoded for html
    # so it looks a little weird, but we are successfully posting
    render :html => "Hello, create data: " + @data
  end

end
