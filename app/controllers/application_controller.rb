class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) do |prms|
      prms.permit( :email, :password, :password_confirmation, :first_name, :last_name, :api_key, :sensor_data)
    end
    devise_parameter_sanitizer.for(:account_update) do |prms|
      prms.permit( :email, :password, :password_confirmation, :current_password, :first_name, :last_name)
    end
  end

   def authenticate_manual
    api_key = request.headers['X-Header-Api-Key']
    @user = User.where(api_key: api_key).first if api_key

    unless @user
      head status: :unauthorized
      return false
    end

   end

end
