class PortalController < ApplicationController

  # Make sure that a user is authenticated before
  # they can access this page.
  before_action :authenticate_user!


    @color_hash = { "1" => [ "rgba(157, 235, 136,0.4)", "#9deb88", "#fff", "#9deb88"], "2" => [ "rgba(137, 137, 235, 0.4)", "#8989eb", "#fff", "#8989eb"], "3" => [ "rgba(70, 191, 189,0.4)", "rgba(70, 191, 189,0.4)", "#fff", "rgba(70, 191, 189)"]}
  
  def index
    return current_user
  end
  
  def room
    return current_user
  end
  
    def get_room_table(nodeID)
    str = "<tr><td>Current Readings</td><td>#{@current_user.get_current_temp_by_node(nodeID)}°C</td><td>#{ @current_user.get_current_humidity_by_node(nodeID)}% </td> <td>#{@current_user.get_current_light_by_node(nodeID)}%</td></tr> <tr><td>Averages</a></td><td>#{@current_user.get_last_hour_temperature_avg(nodeID)}°C</td><td>#{@current_user.get_last_hour_humidity_avg(nodeID)}%</td><td>#{@current_user.get_last_hour_light_avg(nodeID)}%</td> </tr>"

      return str.html_safe
    end
  
  
  def get_quick_stats_table
    @coldest_room = @current_user.get_coldest_room
    @least_humid_room = @current_user.get_least_humid_room
    @brightest_room = @current_user.get_brightest_room
    str = "<tr><td>Room #{@coldest_room}<a href=\"/room/#{@coldest_room}\"><span style=\"font-size:1em; margin-left:1em\" class=\"glyphicon glyphicon-plus\"></span></a></td><td>Room #{@coldest_room}<a href=\"/room/#{@coldest_room}\"><span style=\"font-size:1em; margin-left:1em\" class=\"glyphicon glyphicon-plus\"></span></a></td><td>Room #{@least_humid_room}<a href=\"/room/#{@least_humid_room}\"><span style=\"font-size:1em; margin-left:1em\" class=\"glyphicon glyphicon-plus\"></span></a></td><td>Room #{@brightest_room}<a href=\"/room/#{@brightest_room}\"><span style=\"font-size:1em; margin-left:1em\" class=\"glyphicon glyphicon-plus\"></span></a></td></tr>".html_safe
    return str
  end
  
  def get_index_table
     local_sensor_data = @current_user.get_all_sensor_data 
     str = ""
     local_sensor_data.each do |key, value|
              str= str +  "<tr><td>Room #{key}<a href=\"/room/#{key}\"><span style=\"font-size:1em; margin-left:1em\" class=\"glyphicon glyphicon-plus\"></span></a></td><td>#{@current_user.get_current_temp_by_node(key)}°C </td><td>#{@current_user.get_current_humidity_by_node(key)}% </td><td>#{@current_user.get_current_light_by_node(key)}% </td></tr>"
    end  
    
    return str.html_safe
    
  end
  
  def findRooms
    local_sensor_data = @current_user.get_all_sensor_data 
    @rooms = Array.new()
    local_sensor_data.each do |key, value|
      current_key = key
      @rooms.push(current_key)
    end
    return @rooms
  end
  
  
  
  helper_method :findRooms
  
  
  helper_method :get_index_table
  helper_method :get_room_table
  helper_method :get_quick_stats_table
  
end
