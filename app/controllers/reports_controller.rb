class ReportsController < ApplicationController
before_action :authenticate_user!

  def reports
    return current_user
  end
  
  def findRooms
    local_sensor_data = @current_user.get_all_sensor_data 
    @rooms = Array.new()
    local_sensor_data.each do |key, value|
      current_key = key
      @rooms.push(current_key)
    end
    return @rooms
  end
  
  def generate_javascript_temps
    findRooms
    roomstr = ""
    @rooms.each do |n|
       roomstr += "var temps_room#{n} = #{@current_user.get_last_day_temps_array_by_node(n)};\n"
    end
    return roomstr.html_safe
  end
  
  def generate_javascript_temps_data
    
   @color_hash = [ "rgba(157, 235, 136,0.4)", "rgba(157, 235, 136,1)",  "rgba(244,91,91,0.4)", "rgba(244,91,91,1)", "rgba(119,152,191,0.4)", "rgba(119,152,191,1)", "rgba(170,238,238,0.4)", "rgba(170,238,238,1)", "rgba(238,170,238,0.4)", "rgba(238,170,238,1)", "rgba(157, 235, 136,0.4)", "#9deb88",  "rgba(137, 137, 235, 0.4)", "#8989eb", "rgba(157, 235, 136,0.4)", "#9deb88", "rgba(137, 137, 235, 0.4)", "#8989eb"]
    
    strData= ''
    count = 0
    count_rooms = 0
    @rooms.each do |n|
    strData+= "{label: \"Room #{n}\",fillColor: \"#{@color_hash[count]}\",strokeColor: \"#{@color_hash[count+1]}\",pointColor: \"#{@color_hash[count+1]}\",pointStrokeColor: \"#fff\", pointHighlightFill:\"#fff\", pointHighlightStroke:\"#{@color_hash[count+1]}\", data:  temps_room#{n}}"
    if(count_rooms <= @rooms.length)
        strData += ",\n"
      end
      count += 2
      count_rooms += 1
    end
   return strData.html_safe 
  end
  
   def generate_javascript_humidity
    findRooms
    roomstr = ""
      @rooms.each do |n|
         roomstr += "var humidity_room#{n} = #{@current_user.get_last_day_humidity_array_by_node(n)};\n"
      end
      return roomstr.html_safe
    end
  
  def generate_javascript_humidity_data
    
   @color_hash = [ "rgba(157, 235, 136,0.4)", "rgba(157, 235, 136,1)",  "rgba(244,91,91,0.4)", "rgba(244,91,91,1)", "rgba(119,152,191,0.4)", "rgba(119,152,191,1)", "rgba(170,238,238,0.4)", "rgba(170,238,238,1)", "rgba(238,170,238,0.4)", "rgba(238,170,238,1)", "rgba(157, 235, 136,0.4)", "#9deb88",  "rgba(137, 137, 235, 0.4)", "#8989eb", "rgba(157, 235, 136,0.4)", "#9deb88", "rgba(137, 137, 235, 0.4)", "#8989eb"]
  
    
    strData= ''
    count = 0
    count_rooms = 0
    @rooms.each do |n|
    strData+= "{label: \"Room #{n}\",fillColor: \"#{@color_hash[count]}\",strokeColor: \"#{@color_hash[count+1]}\",pointColor: \"#{@color_hash[count+1]}\",pointStrokeColor: \"#fff\", pointHighlightFill:\"#fff\", pointHighlightStroke:\"#{@color_hash[count+1]}\", data: humidity_room#{n}}"
    if(count_rooms <= @rooms.length)
        strData += ",\n"
      end
      count += 2
      count_rooms += 1
    end
   return strData.html_safe 
  end
  
  def generate_javascript_light
    findRooms
    roomstr = ""
      @rooms.each do |n|
         roomstr += "var light_room#{n} = #{@current_user.get_last_day_light_array_by_node(n)};\n"
      end
      return roomstr.html_safe
    end
  
  def generate_javascript_light_data
    #                                                                                             new                                                      new                                                                                           new                                                            new                                                                   new                                               new                                                     new
   @color_hash = [ "rgba(157, 235, 136,0.4)", "rgba(157, 235, 136,1)",  "rgba(244,91,91,0.4)", "rgba(244,91,91,1)", "rgba(119,152,191,0.4)", "rgba(119,152,191,1)", "rgba(170,238,238,0.4)", "rgba(170,238,238,1)", "rgba(238,170,238,0.4)", "rgba(238,170,238,1)", "rgba(157, 235, 136,0.4)", "#9deb88",  "rgba(137, 137, 235, 0.4)", "#8989eb", "rgba(157, 235, 136,0.4)", "#9deb88", "rgba(137, 137, 235, 0.4)", "#8989eb"]
    
    strData= ''
    count = 0
    count_rooms = 0
    @rooms.each do |n|
          strData+= "{label: \"Room #{n}\",fillColor: \"#{@color_hash[count]}\",strokeColor: \"#{@color_hash[count+1]}\",pointColor: \"#{@color_hash[count+1]}\",pointStrokeColor: \"#fff\", pointHighlightFill:\"#fff\", pointHighlightStroke:\"#{@color_hash[count+1]}\", data:  light_room#{n}}"
      if(count_rooms <= @rooms.length)
        strData += ",\n"
      end
      count += 2
      count_rooms += 1
      
    end
   return strData.html_safe 
  end
  
  
  helper_method :findRooms
  helper_method :generate_javascript_temps
  helper_method :generate_javascript_temps_data
  helper_method :generate_javascript_humidity
  
  helper_method :generate_javascript_humidity_data
  
  helper_method :generate_javascript_light
  
  helper_method :generate_javascript_light_data

  
end