module DeviseHelper
    def devise_error_messages!
        return '' if resource.errors.empty?
        
        messages = resource.errors.full_messages.map { |msg| content_tag(:li, msg)}.join
        html = <<-HTML
         <paper-toast text="#{messages}" duration="0" opened>
  
  
</paper-toast>
        
        
        
        HTML
        
        html.html_safe
    end
end

        