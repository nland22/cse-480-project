class User
  include Mongoid::Document
  include Mongoid::Attributes
  include Mongoid::Attributes::Dynamic
  
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  before_create do |doc|
    doc.api_key = doc.generate_api_key
  end

  ## Database authenticatable
  field :email,              type: String, default: ""
  field :encrypted_password, type: String, default: ""
  field :first_name,         type: String, default: ""
  field :last_name,          type: String, default: ""
  field :api_key,            type: String, default: ""
  field :use_c,              type: Boolean, default: true
  field :sensor_data,        type: Hash, default: {}
  
  

  validates_presence_of :email
  validates_presence_of :encrypted_password
  validates_presence_of :first_name
  validates_presence_of :last_name


  ## Recoverable
  field :reset_password_token,   type: String
  field :reset_password_sent_at, type: Time

  ## Rememberable
  field :remember_created_at, type: Time

  ## Trackable
  field :sign_in_count,      type: Integer, default: 0
  field :current_sign_in_at, type: Time
  field :last_sign_in_at,    type: Time
  field :current_sign_in_ip, type: String
  field :last_sign_in_ip,    type: String

  ## Confirmable
  # field :confirmation_token,   type: String
  # field :confirmed_at,         type: Time
  # field :confirmation_sent_at, type: Time
  # field :unconfirmed_email,    type: String # Only if using reconfirmable

  ## Lockable
  # field :failed_attempts, type: Integer, default: 0 # Only if lock strategy is :failed_attempts
  # field :unlock_token,    type: String # Only if unlock strategy is :email or :both
  # field :locked_at,       type: Time

  ##api_key

  
  
  def get_all_sensor_data
    return read_attribute(("sensor_data").to_sym)
  end
  
  def get_sensor_data_by_node(value)
    return read_attribute(( "sensor_data."+ value.to_s).to_sym)
  end
  
  def get_current_temp_by_node(value)
    @data_object = get_sensor_data_by_node(value)
    if(@data_object.has_key?(:readings))
      @readings = @data_object[:readings]
      @length = @readings.length - 1
      last = @readings[@length]
      return @current_temp = last[:temperature] /100.00
    end
  end
  
  def get_current_light_by_node(value)
    @data_object = get_sensor_data_by_node(value)
    if(@data_object.has_key?(:readings))
      @readings = @data_object[:readings]
      @length = @readings.length - 1
      last = @readings[@length]
      return @current_light = last[:light] /10.00
    end
  end
  
  def get_current_humidity_by_node(value)
    @data_object = get_sensor_data_by_node(value)
    if(@data_object.has_key?(:readings))
      @readings = @data_object[:readings]
      @length = @readings.length - 1
      last = @readings[@length]
      return @current_light = last[:humidity] / 100.00
    end
  end
  
  
  def get_current_temp_avg()
    all_sensor_data = get_all_sensor_data()
    node_count = 0
    total_temp = 0
    all_sensor_data.each do |key, value|
      if(value.has_key?(:readings))
        readings = value[:readings]
        node_count = node_count + 1
        readings_length = readings.length - 1
        last = readings[readings_length]
        total_temp = total_temp + last[:temperature]
      end
    end
    if(node_count > 0)
      return ((total_temp/node_count) / 100.00)
    else 
      return 0
    end
  end
  
  def get_current_humidity_avg()
    all_sensor_data = get_all_sensor_data()
    node_count = 0
    total_hum = 0
    all_sensor_data.each do |key, value|
      if(value.has_key?(:readings))
        readings = value[:readings]
        node_count = node_count + 1
        readings_length = readings.length - 1
        last = readings[readings_length]
        total_hum = total_hum + last[:humidity]
      end
    end
    if(node_count > 0 )
      return ((total_hum/node_count) / 100.00)
    else 
      return 0
    end
  end
  
  def get_current_light_avg()
    all_sensor_data = get_all_sensor_data()
    node_count = 0
    total_light = 0
    all_sensor_data.each do |key, value|
      if(value.has_key?(:readings))
        readings = value[:readings]
        node_count = node_count + 1
        readings_length = readings.length - 1
        last = readings[readings_length]
        total_light = total_light + last[:light]
      end
    end
    if(node_count > 0 )
      return ((total_light/node_count) / 10.00)
    else 
      return 0
    end
  end
  
  def get_last_hour_temperature_avg(nodeid)
    
    node_sensor_data = get_sensor_data_by_node(nodeid)
    entry_count = 0
    
    if(node_sensor_data.has_key?(:readings))
      readings = node_sensor_data[:readings]
      
      total_temp = 0
      readings_length = readings.length
      end_index = readings_length - 1 
      start_index = 0  
      
        if(readings_length < 30)
          start_index = 0
          entry_count = readings_length
        else
          start_index = readings_length - 30
          entry_count = 30
        end
        
        for i in start_index..end_index
          total_temp += readings[i][:temperature]
        end
    end
    
    if(entry_count > 0 )
      return ((total_temp/entry_count) / 100.00)
    else 
      return 0
    end
  
  end
  
  def get_x_hours_temperature_avg(nodeid, hours)
    
    node_sensor_data = get_sensor_data_by_node(nodeid)
    
    if(node_sensor_data.has_key?(:readings))
      readings = node_sensor_data[:readings]
      entry_count = 0
      total_temp = 0
      readings_length = readings.length
      end_index = readings_length - 1 
      start_index = 0  
      
        if(readings_length < 30 * hours)
          start_index = 0
          entry_count = readings_length
        else
          start_index = readings_length - 30 * hours
          entry_count = 30 * hours
        end
        
        for i in start_index..end_index
          total_temp += readings[i][:temperature]
        end
    end
    
    if(entry_count > 0 )
      return ((total_temp/entry_count) / 100.00)
    else 
      return 0
    end
  
  end
  
  def get_last_hour_humidity_avg(nodeid)
    
    node_sensor_data = get_sensor_data_by_node(nodeid)
    entry_count = 0
      
    if(node_sensor_data.has_key?(:readings))
      readings = node_sensor_data[:readings]
    
      total_humidity = 0
      readings_length = readings.length
      end_index = readings_length - 1 
      start_index = 0  
      
        if(readings_length < 30)
          start_index = 0
          entry_count = readings_length
        else
          start_index = readings_length - 30
          entry_count = 30
        end
        
        for i in start_index..end_index
          total_humidity += readings[i][:humidity]
        end
    end
    
    if(entry_count > 0 )
      return ((total_humidity/entry_count) / 100.00)
    else 
      return 0
    end
  
  end
  
  def get_x_hours_humidity_avg(nodeid, hours)
    
    node_sensor_data = get_sensor_data_by_node(nodeid)
    entry_count = 0
    
    if(node_sensor_data.has_key?(:readings))
      readings = node_sensor_data[:readings]
      
      total_humidity = 0
      readings_length = readings.length
      end_index = readings_length - 1 
      start_index = 0  
      
        if(readings_length < 30 * hours)
          start_index = 0
          entry_count = readings_length
        else
          start_index = readings_length - 30 * hours
          entry_count = 30 * hours
        end
        
        for i in start_index..end_index
          total_humidity += readings[i][:humidity]
        end
    end
    
    if(entry_count > 0 )
      return ((total_humidity/entry_count) / 100.00)
    else 
      return 0
    end
  
  end
  
  def get_last_hour_light_avg(nodeid)
    
    node_sensor_data = get_sensor_data_by_node(nodeid)
    entry_count = 0
    
    if(node_sensor_data.has_key?(:readings))
      readings = node_sensor_data[:readings]
      
      total_light = 0
      readings_length = readings.length
      end_index = readings_length - 1 
      start_index = 0  
      
        if(readings_length < 30)
          start_index = 0
          entry_count = readings_length
        else
          start_index = readings_length - 30
          entry_count = 30
        end
        
        for i in start_index..end_index
          total_light += readings[i][:light]
        end
    end
    
    if(entry_count > 0 )
      return ((total_light/entry_count) / 10.00)
    else 
      return 0
    end
  
  end
  
  def get_x_hours_light_avg(nodeid, hours)
    
    node_sensor_data = get_sensor_data_by_node(nodeid)
    entry_count = 0
    
    if(node_sensor_data.has_key?(:readings))
      readings = node_sensor_data[:readings]
      
      total_light = 0
      readings_length = readings.length
      end_index = readings_length - 1 
      start_index = 0  
      
        if(readings_length < 30 * hours)
          start_index = 0
          entry_count = readings_length
        else
          start_index = readings_length - 30 * hours
          entry_count = 30 * hours
        end
        
        for i in start_index..end_index
          total_light += readings[i][:light]
        end
    end
    
    if(entry_count > 0 )
      return ((total_light/entry_count) / 10.00)
    else 
      return 0
    end
  
  end
  
  def get_last_day_temps_by_node(nodeid)
    
    min_entries_needed = 30 * 24
    
    node_sensor_data = get_sensor_data_by_node(nodeid)
    
    if(node_sensor_data.has_key?(:readings))
      if(node_sensor_data[:readings].length >= min_entries_needed)
        results_hash = Hash.new
        readings = node_sensor_data[:readings]
        start_index = readings_length - min_entries_needed
        counter = 1
        (start_index..readings_length).step(30) do |n|
          results_hash[counter.to_s.to_sym] = readings[n][:temperature] / 100.00
          counter += 1
        end
        return results_hash
      else
        return {}
      end
    else
      return {}
    end
    
    
  end
  
  def get_last_day_temps_array_by_node(nodeid)
    
    #min_entries_needed = 30 * 24
    min_entries_needed = 24
    
    node_sensor_data = get_all_sensor_data
    
    node_sensor_data = node_sensor_data[nodeid.to_s.to_sym]
    
    if(node_sensor_data.has_key?(:readings))
      if(node_sensor_data[:readings].length >= min_entries_needed)
        results_array = Array.new
        readings = node_sensor_data[:readings]
        readings_length = readings.length - 1
        start_index = readings_length - min_entries_needed
        
        readings.last(24).each do |n|
          results_array.push(n[:temperature] / 100.00)
        end
        return results_array
      else
        return []
      end
    else
      return []
    end
    
    
  end

  def get_last_day_humidity_array_by_node(nodeid)
    
    #min_entries_needed = 24 * 30
    min_entries_needed = 24
    
    node_sensor_data = get_sensor_data_by_node(nodeid)
    
    if(node_sensor_data.has_key?(:readings))
      if(node_sensor_data[:readings].length >= min_entries_needed)
        results_array = Array.new
        readings = node_sensor_data[:readings]
        readings.last(24).each do |n|
          results_array.push(n[:humidity] / 100.00)
        end
        return results_array
      else
        return []
      end
    else
      return []
    end
    
    
  end

  def get_last_day_light_array_by_node(nodeid)
    
    #min_entries_needed = 24 * 30
    min_entries_needed = 24
    
    node_sensor_data = get_sensor_data_by_node(nodeid)
    
    if(node_sensor_data.has_key?(:readings))
      if(node_sensor_data[:readings].length >= min_entries_needed)
        results_array = Array.new
        readings = node_sensor_data[:readings]
        readings.last(24).each do |n|
          results_array.push(n[:light] / 10.00)
        end
        return results_array
      else
        return []
      end
    else
      return []
    end
    
    
  end

  
  def get_last_half_day_temps_by_node(nodeid)
    
    min_entries_needed = 30 * 12
    
    node_sensor_data = get_sensor_data_by_node(nodeid)
    
    if(node_sensor_data.has_key?(:readings))
      if(node_sensor_data[:readings].length >= min_entries_needed)
        results_hash = Hash.new
        readings = node_sensor_data[:readings]
        start_index = readings_length - min_entries_needed
        counter = 1
        (start_index..readings_length).step(30) do |n|
          results_hash[counter.to_s.to_sym] = readings[n][:temperature]
          counter += 1
        end
        return results_hash
      else
        return {}
      end
    else
      return {}
    end
    
    
  end
  
  def get_coldest_room()
    all_sensor_data = get_all_sensor_data
    coldest_node = 0
    lowest_temp = 3000
    current_temp = 0 
    all_sensor_data.each do |key, value|
      if(value.has_key?(:readings))
        current_temp = get_last_hour_temperature_avg(key)
        if(current_temp < lowest_temp)
          lowest_temp = current_temp
          coldest_node = key
        end
      end
    end
    return coldest_node
  end
  
  def get_least_humid_room()
    all_sensor_data = get_all_sensor_data
    least_humid_node = 0
    lowest_humidity = 3000
    current_humidity = 0 
    all_sensor_data.each do |key, value|
      if(value.has_key?(:readings))
        current_humidity = get_last_hour_humidity_avg(key)
        if(current_humidity < lowest_humidity)
          lowest_humidity = current_humidity
          least_humid_node = key
        end
      end
    end
    return least_humid_node
  end
  
  def get_darkest_room()
    all_sensor_data = get_all_sensor_data
    darkest_node = 0
    lowest_light = 3000
    current_light = 0 
    all_sensor_data.each do |key, value|
      if(value.has_key?(:readings))
        current_light = get_last_hour_light_avg(key)
        if(current_light < lowest_light)
          lowest_light = current_light
          darkest_node = key
        end
      end
    end
    return darkest_node
  end
  
  def get_brightest_room()
    all_sensor_data = get_all_sensor_data
    brightest_node = 0
    brightest_light = -1
    current_light = 0 
    all_sensor_data.each do |key, value|
      if(value.has_key?(:readings))
        current_light = get_last_hour_light_avg(key)
        if(current_light > brightest_light)
          brightest_light = current_light
          brightest_node = key
        end
      end
    end
    return brightest_node
  end
  
  def generate_api_key
    loop do
      token = SecureRandom.base64.tr('+/=', 'Qrt')
      break token unless User.where(:api_key => token).exists?
   end
  end

  #attr_accessible :first_name, :last_name, :email, :password, :password_confirmation, :remember_me, :created_at, :updated_at

    def user_params
      params.require(:user).permit(:first_name, :last_name, :api_key, :email, :password, :password_confirmation)
    end

end
