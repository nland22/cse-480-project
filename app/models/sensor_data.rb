require "json"

class SensorData
  include Mongoid::Document
  include Mongoid::Timestamps::Created
  include Mongoid::Attributes::Dynamic
  
  
  def write_sensor_data_to_user(data, id, node)
    Rails.logger.debug "Write Sensor ... " + data
    data = CGI::unescape(data)
    data = data.gsub("\n", '')
    data = data.gsub("\r", '')
    data = data.gsub("\t", '')
   
    node = CGI::unescape(node)
    node = node.gsub("\n", '')
    node = node.gsub("\r", '')
    node = node.gsub("\t", '')
   
    Rails.logger.debug "Write Sensor ... " + data
    
    json = JSON.parse(data)
    # time = Time.now
    @user = User.where(_id: id)
    
    # @counter =  @user.update("$inc" => {"sensor_data." + node + ".count" => 1 })
    #   @counter = @user.first().attributes()
    #   Rails.logger.debug @counter
    
    # if validate_user(@user, api_key)
      return  @user.update("$push": {"sensor_data.#{node}.readings": {"$each": [json]}})
    # else
    #   return "Invalid user"
    # end
  end

  def validate_user(user, api_key)
    return api_key == user.api_key
  end
end
