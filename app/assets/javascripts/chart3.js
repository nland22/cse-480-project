$(function () {
    $('#area1').highcharts({
        chart: {
            type: 'areaspline'
        },
        title: {
            text: 'Average Temperature during one week'
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'top',
            x: 150,
            y: 100,
            floating: true,
            borderWidth: 1,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        xAxis: {
            categories: [
                'Monday',
                'Tuesday',
                'Wednesday',
                'Thursday',
                'Friday',
                'Saturday',
                'Sunday'
            ]
        },
        yAxis: {
            floor: 50,
            ceiling: 100,
            title: {
                text: 'Temperature'
            },
            labels: {
                format: '{value}°F',
            }
        },
        tooltip: {
            shared: true,
            valueSuffix: ' °F'
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            areaspline: {
                fillOpacity: 0.5
            }
        },
        series: [{
            name: 'Living Room',
            data: [65.2, 67.4, 63, 68.1, 69.2, 71.1, 70.8]
        }
        , {
            name: 'Bedroom',
            data: [67.3, 66.1, 62.3, 69.1, 68.2, 72.1, 73.8]
        }
        ]
    });
});

$(function () {

    var ranges = [
            [1246406400000, 14.3, 27.7],
            [1246492800000, 14.5, 27.8],
            [1246579200000, 15.5, 29.6],
            [1246665600000, 16.7, 30.7],
            [1246752000000, 16.5, 25.0],
            [1246838400000, 17.8, 25.7],
            [1246924800000, 13.5, 24.8],
            [1247011200000, 10.5, 21.4],
            [1247097600000, 9.2, 23.8],
            [1247184000000, 11.6, 21.8],
            [1247270400000, 10.7, 23.7],
            [1247356800000, 11.0, 23.3],
            [1247443200000, 11.6, 23.7],
            [1247529600000, 11.8, 20.7],
            [1247616000000, 12.6, 22.4],
            [1247702400000, 13.6, 19.6],
            [1247788800000, 11.4, 22.6],
            [1247875200000, 13.2, 25.0],
            [1247961600000, 14.2, 21.6],
            [1248048000000, 13.1, 17.1],
            [1248134400000, 12.2, 15.5],
            [1248220800000, 12.0, 20.8],
            [1248307200000, 12.0, 17.1],
            [1248393600000, 12.7, 18.3],
            [1248480000000, 12.4, 19.4],
            [1248566400000, 12.6, 19.9],
            [1248652800000, 11.9, 20.2],
            [1248739200000, 11.0, 19.3],
            [1248825600000, 10.8, 17.8],
            [1248912000000, 11.8, 18.5],
            [1248998400000, 10.8, 16.1]
        ],
        averages = [
            [1246406400000, 21.5],
            [1246492800000, 22.1],
            [1246579200000, 23],
            [1246665600000, 23.8],
            [1246752000000, 21.4],
            [1246838400000, 21.3],
            [1246924800000, 18.3],
            [1247011200000, 15.4],
            [1247097600000, 16.4],
            [1247184000000, 17.7],
            [1247270400000, 17.5],
            [1247356800000, 17.6],
            [1247443200000, 17.7],
            [1247529600000, 16.8],
            [1247616000000, 17.7],
            [1247702400000, 16.3],
            [1247788800000, 17.8],
            [1247875200000, 18.1],
            [1247961600000, 17.2],
            [1248048000000, 14.4],
            [1248134400000, 13.7],
            [1248220800000, 15.7],
            [1248307200000, 14.6],
            [1248393600000, 15.3],
            [1248480000000, 15.3],
            [1248566400000, 15.8],
            [1248652800000, 15.2],
            [1248739200000, 14.8],
            [1248825600000, 14.4],
            [1248912000000, 15],
            [1248998400000, 13.6]
        ];


    $('#range').highcharts({

        title: {
            text: 'July temperatures'
        },

        xAxis: {
            type: 'Time'
        },

        yAxis: {
            title: {
                text: null
            },
            labels: {
                format: '{value}°F',
            }
        },

        tooltip: {
            crosshairs: true,
            shared: true,
            valueSuffix: '°C'
        },

        legend: {
        },

        series: [{
            name: 'Temperature',
            data: averages,
            zIndex: 1,
            marker: {
                fillColor: 'white',
                lineWidth: 2,
                lineColor: Highcharts.getOptions().colors[0]
            }
        }, {
            name: 'Range',
            data: ranges,
            type: 'arearange',
            lineWidth: 0,
            linkedTo: ':previous',
            color: Highcharts.getOptions().colors[0],
            fillOpacity: 0.3,
            zIndex: 0
        }]
    });
});

$(function () {
    $('#area2').highcharts({
        chart: {
            type: 'areaspline'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: ['12 AM', '1 AM', '2AM', '3AM', '4 AM',  '5 AM', '6 AM', '7 AM',  '8 AM', '9 AM',  '10 AM', '11 AM',
	                     '12 PM', '1 PM', '2PM', '3PM', '4 PM',  '5 PM', '6 PM', '7 PM',  '8 PM', '9 PM',  '10 PM', '11 PM'
                ],
            tickmarkPlacement: 'on',
            title: {
                enabled: false
            }
        },
        yAxis: {
            floor: 50,
            ceiling: 100,
            title: {
                text: 'Temperature'
            },
            labels: {
                format: '{value} °F',
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y:,.0f} °F<br/>',
            shared: true
        },
        plotOptions: {
            area: {
                //stacking: 'percent',
                lineColor: '#ffffff',
                lineWidth: 1,
                marker: {
                    lineWidth: 1,
                    lineColor: '#ffffff'
                }
            }
        },
        series: [{
            name: 'Living room',
            data: [67,68,69,68,67,68,65,67,65,64,67,66,68,68,67,69,70,71,72,73,70,69,68,68]
        }, {
            name: 'Bedroom 1',
            data: [74,67,77,75,76,71,69,67,77,67,71,72,68,72,76,73,74,72,75,73,76,72,75,73]
        }, {
            name: 'Bedroom 2',
            data: [76,70,74,71,74,68,72,69,68,70,70,69,72,75,73,76,72,71,71,77,71,69,76,70]
        }, {
            name: 'Basement',
            data: [64,59,60,58,66,59,56,58,61,55,60,67,60,56,65,55,63,67,60,56,65,58,66,59]
        }, {
            name: 'Kitchen',
            data: [73,74,68,69,70,72,74,73,73,67,78,74,74,74,70,77,77,67,77,71,70,76,75,78]
        }]
    });
});

$(function () {
    $(document).ready(function () {
      Highcharts.setOptions({
            global: {
                useUTC: false
            }
      });

        $('#livedata2').highcharts({
            chart: {
                type: 'spline',
                animation: Highcharts.svg, // don't animate in old IE
                marginRight: 10,
                events: {
                    load: function () {

                        // set up the updating of the chart each second
                        var series1 = this.series[0];
                        setInterval(function () {
                            var x = (new Date()).getTime(), // current time
                                y = (Math.random() * 20) + 65;
                            series1.addPoint([x, y], true, true);
                        }, 5000);
                        
                        
                    }
                }
            },
            title: {
                text: 'Live temperature data'
            },
            xAxis: {
                type: 'datetime',
                tickPixelInterval: 150
            },
            yAxis: {
                title: {
                    text: 'Value'
                },
                plotLines: [{
                    value: 10,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + '</b><br/>' +
                        Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
                        Highcharts.numberFormat(this.y, 2);
                }
            },
            legend: {
                enabled: true
            },
            exporting: {
                enabled: false
            },
            series: [
              {
                name: 'Room1',
                data: (function () {
                    // generate an array of random data
                    var data1 = [],
                        time = (new Date()).getTime(),
                        i;

                    for (i = -19; i <= 0; i += 1) {
                        data1.push({
                            x: time + i * 3000,
                            y: (Math.random() * 20) + 65,
                        });
                    }
                    return data1;
                }())
            }
            ]
        });
    });
});



$(function () {
    $(document).ready(function () {
      Highcharts.setOptions({
            global: {
                useUTC: false
            }
      });

        $('#livedata3').highcharts({
            chart: {
                type: 'area',
                animation: Highcharts.svg, // don't animate in old IE
                marginRight: 10,
                events: {
                    load: function () {

                        // set up the updating of the chart each second
                        var series1 = this.series[0];
                        setInterval(function () {
                            var x = (new Date()).getTime(), // current time
                                y = (Math.random() * 20) + 65;
                            series1.addPoint([x, y], true, true);
                        }, 5000);
                        
                        
                    }
                }
            },
            title: {
                text: 'Live humidity data'
            },
            xAxis: {
                type: 'datetime',
                tickPixelInterval: 150
            },
            yAxis: {
                title: {
                    text: 'Value'
                },
                plotLines: [{
                    value: 10,
                    width: 1,
                }]
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + '</b><br/>' +
                        Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
                        Highcharts.numberFormat(this.y, 2);
                }
            },
            legend: {
                enabled: true
            },
            exporting: {
                enabled: false
            },
            series: [
              {
                name: 'Room1',
                data: (function () {
                    // generate an array of random data
                    var data1 = [],
                        time = (new Date()).getTime(),
                        i;

                    for (i = -19; i <= 0; i += 1) {
                        data1.push({
                            x: time + i * 3000,
                            y: (Math.random() * 100) + 10,
                        });
                    }
                    return data1;
                }())
            }
            ]
        });
    });
});

$(function () {
    $(document).ready(function () {
      Highcharts.setOptions({
            global: {
                useUTC: false
            }
      });

        $('#livedata4').highcharts({
            chart: {
                type: 'column',
                animation: Highcharts.svg, // don't animate in old IE
                marginRight: 10,
                events: {
                    load: function () {

                        // set up the updating of the chart each second
                        var series1 = this.series[0];
                        setInterval(function () {
                            var x = (new Date()).getTime(), // current time
                                y = (Math.random() * 1000) + 65;
                            series1.addPoint([x, y], true, true);
                        }, 5000);
                        
                        
                    }
                }
            },
            title: {
                text: 'Live light data'
            },
            xAxis: {
                type: 'datetime',
                tickPixelInterval: 150
            },
            yAxis: {
                title: {
                    text: 'Value'
                },
                plotLines: [{
                    value: 10,
                    width: 1,
                }]
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + '</b><br/>' +
                        Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
                        Highcharts.numberFormat(this.y, 2);
                }
            },
            legend: {
                enabled: true
            },
            exporting: {
                enabled: false
            },
            series: [
              {
                name: 'Room1',
                data: (function () {
                    // generate an array of random data
                    var data1 = [],
                        time = (new Date()).getTime(),
                        i;

                    for (i = -19; i <= 0; i += 1) {
                        data1.push({
                            x: time + i * 3000,
                            y: (Math.random() * 1000) + 10,
                        });
                    }
                    return data1;
                }())
            }
            ]
        });
    });
});


$(function () {
    $('#combo').highcharts({
        chart: {
            // height: 400,
            marginTop: 100
        },
        title: {
            text: 'Combination Temp chart'
        },
        xAxis: {
            categories: ['Bedroom 1', 'Bedroom 2', 'Living Room', 'Kitchen', 'Basement']
        },
        yAxis: { // Primary yAxis
            floor: 0,
            ceiling: 120,
            title: {text: 'Temperature'},
            labels: { format: '{value}°F'},
        },    
        series: [{
            type: 'column',
            name: 'Today',
            data: [67, 68, 67, 69, 62]
        }, {
            type: 'column',
            name: 'Yesteday',
            data: [69, 70, 68, 71, 63]
        },  {
            type: 'spline',
            name: 'Average Temp',
            data: [68, 69, 65, 72, 61],
            marker: {
                lineWidth: 2,
                lineColor: Highcharts.getOptions().colors[3],
                fillColor: 'white'
            }
        }, {
            type: 'pie',
            name: 'Total Temp',
            data: [{
                name: 'Bedroom 1',
                y: 138/100,
                color: Highcharts.getOptions().colors[5] // Jane's color
            }, {
                name: 'Bedroom 2',
                y: 138/100,
                color: Highcharts.getOptions().colors[6] // John's color
            }, {
                name: 'Living room',
                y: 135/100,
                color: Highcharts.getOptions().colors[2] // Joe's color
            }, {
                name: 'Kitchen',
                y: 140/100,
                color: Highcharts.getOptions().colors[3] // Joe's color
            }, {
                name: 'Basement',
                y: 125/100,
                color: Highcharts.getOptions().colors[4] // Joe's color
            }],
            center: [50, -30],
            size: 80,
            showInLegend: false,
            dataLabels: {
                enabled: false
            }
        }]
    });
});



$(function () {

    // Parse the data from an inline table using the Highcharts Data plugin
    $('#windrose').highcharts({
        data: {
            table: 'temp',
            startRow: 1,
            endRow: 24,
            endColumn: 7
        },

        chart: {
            polar: true,
            type: 'column'
        },

        title: {
            text: 'Temperature Chart'
        },

        subtitle: {
            text: 'None'
        },

        pane: {
            size: '90%'
        },

        legend: {
            align: 'right',
            verticalAlign: 'top',
            y: 100,
            layout: 'vertical'
        },

        xAxis: {
            tickmarkPlacement: 'on'
        },

        yAxis: {
            min: 0,
            endOnTick: false,
            showLastLabel: true,
            title: {
                text: 'Temperature (°F)'
            },
            labels: {
                formatter: function () {
                    return this.value + '°F';
                }
            },
            reversedStacks: true
        },

        tooltip: {
            valueSuffix: '°F'
        },

        plotOptions: {
            series: {
                stacking: 'normal',
                shadow: false,
                groupPadding: -.1,
                pointPlacement: 'on'
            }
        }
    });
});



$(function () {

    // Parse the data from an inline table using the Highcharts Data plugin
    $('#chart8').highcharts({
        data: {
            table: 'humidity',
            startRow: 1,
            endRow: 24,
            endColumn: 7
        },

        chart: {
            polar: true,
            type: 'column'
        },

        title: {
            text: 'Humidity Chart'
        },

        subtitle: {
            text: 'None'
        },

        pane: {
            size: '90%'
        },

        legend: {
            align: 'right',
            verticalAlign: 'top',
            y: 100,
            layout: 'vertical'
        },

        xAxis: {
            tickmarkPlacement: 'on'
        },

        yAxis: {
            min: 0,
            endOnTick: false,
            showLastLabel: true,
            title: {
                text: 'Temperature (°F)'
            },
            labels: {
                formatter: function () {
                    return this.value + '°F';
                }
            },
            reversedStacks: true
        },

        tooltip: {
            valueSuffix: '°F'
        },

        plotOptions: {
            series: {
                stacking: 'normal',
                shadow: false,
                groupPadding: -.1,
                pointPlacement: 'on'
            }
        }
    });
});