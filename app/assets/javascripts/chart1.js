
function drawChart1() {
  var randomTemp1 = function() { return Math.floor(Math.random() * 20) + 60;}
  var randomTemp2 = function() { return Math.floor(Math.random() * 10) + 60;}
  var randomTemp3 = function() { return Math.floor(Math.random() * 20) + 65;}
  var randomTemp4 = function() { return Math.floor(Math.random() * 10) + 65;}

  var chart1Data = {
 
	labels : ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
	datasets : [
		{
		    label: "Weekend",
			fillColor : "rgba(172,194,132,0.4)",
			strokeColor : "#ACC26D",
			pointColor : "#fff",
			pointStrokeColor : "#449D44",
			data : [randomTemp1(), randomTemp1(), randomTemp1(),randomTemp1(), randomTemp1(), randomTemp1(), randomTemp1(), randomTemp1(), randomTemp1(), randomTemp1(),randomTemp1(),randomTemp1()]
		}
		,
		{
		    label: "Weeknights",
			fillColor : "rgba(172,194,132,0.4)",
			strokeColor : "#ACC26D",
			pointColor : "#fff",
			pointStrokeColor : "#88E585",
			data : [randomTemp4(), randomTemp4(), randomTemp4(),randomTemp4(), randomTemp4(), randomTemp4(), randomTemp4(), randomTemp4(), randomTemp4(), randomTemp4(),randomTemp4(),randomTemp4()]
		}
		,
		{
		    label: "Weekdays",
			fillColor : "rgba(21,76,29,0.4)",
			strokeColor : "#49D45E",
			pointColor : "#fff",
			pointStrokeColor : "#49D45E",
			data : [randomTemp2(), randomTemp2(), randomTemp2(),randomTemp2(), randomTemp2(), randomTemp2(), randomTemp2(), randomTemp2(), randomTemp2(), randomTemp2(),randomTemp2(),randomTemp2()]
		}
		,
		{
		    label: "Weekend days",
			fillColor : "rgba(172,194,132,0.4)",
			strokeColor : "#ACC26D",
			pointColor : "#fff",
			pointStrokeColor : "#A4D496",
			data : [randomTemp3(), randomTemp3(), randomTemp3(),randomTemp3(), randomTemp3(), randomTemp3(), randomTemp3(), randomTemp3(), randomTemp3(), randomTemp3(),randomTemp3(),randomTemp3()]
		}

	]
}
  var chart1 = document.getElementById('chart1').getContext('2d');
  var chart1 = new Chart(chart1).Line(chart1Data, 
  									  {   responsive : true
  										//, scaleBeginAtZero : false
  										//, animateScale : true
  										, showTooltips: false
  									//	, showScale: true
  										//, scaleShowLine : true
  										});
	    legend(document.getElementById("chart1Legend"), chart1Data);
}

function drawChart2() {
  var randomTemp1 = function() { return Math.floor(Math.random() * 20) + 60;}
  var randomTemp2 = function() { return Math.floor(Math.random() * 10) + 60;}
  var randomTemp3 = function() { return Math.floor(Math.random() * 20) + 65;}
  var randomTemp4 = function() { return Math.floor(Math.random() * 10) + 65;}
  var randomTemp5 = function() { return Math.floor(Math.random() * 10) + 50;}
  
  var chart2Data = {
      labels: ['This Week', 'Last Week', 'Month Average'],
      datasets: [
      	  {
              label: 'Whole house',
              fillColor: '#9deb88',
              data: [randomTemp1(), randomTemp1(), randomTemp1()]
          },
          {
              label: 'Bedroom 1',
              fillColor: '#46BFBD',
              data: [randomTemp4(), randomTemp4(), randomTemp4()]
          },
          {
              label: 'Bedroom 2',
              fillColor: '#46BFBD',
              data: [randomTemp4(), randomTemp4(), randomTemp4()]
          }
          ,
          {
              label: 'Livingroom',
              fillColor: '#46BFBD',
              data: [randomTemp1(), randomTemp1(), randomTemp1()]
          }
          ,
          {
              label: 'Kitchen',
              fillColor: '#46BFBD',
              data: [randomTemp3(), randomTemp3(), randomTemp3()]
          }
                    ,
          {
              label: 'Basement',
              fillColor: '#46BFBD',
              data: [randomTemp5(), randomTemp5(), randomTemp5()]
          }
      ]
  };
  var chart2 = document.getElementById('chart2').getContext('2d');
  var chart2 = new Chart(chart2).Bar(chart2Data, {	responsive : true	});
	    legend(document.getElementById("chart2Legend"), chart2Data);
}

function drawChart3(){
  var randomScalingFactor2 = function() { return Math.floor(Math.random() * 20) + 65;}
	var chart3Data = {
		labels : ["January","February","March","April","May","June","July"],
		datasets : [
			{
			  label : "Today",
				fillColor : "rgba(0,220,220,0.5)",
				strokeColor : "rgba(0,220,220,0.8)",
				highlightFill: "rgba(0,220,220,0.75)",
				highlightStroke: "rgba(0,220,220,1)",
				data : [randomScalingFactor2(),randomScalingFactor2(),randomScalingFactor2(),randomScalingFactor2(),randomScalingFactor2(),randomScalingFactor2(),randomScalingFactor2()]
			},
			{
			  label : "March 1st",
				fillColor : "rgba(151,187,205,0.5)",
				strokeColor : "rgba(151,187,205,0.8)",
				highlightFill : "rgba(151,187,205,0.75)",
				highlightStroke : "rgba(151,187,205,1)",
				data : [randomScalingFactor2(),randomScalingFactor2(),randomScalingFactor2(),randomScalingFactor2(),randomScalingFactor2(),randomScalingFactor2(),randomScalingFactor2()]
			}
		]
	}
	var chart3 = document.getElementById("chart3").getContext("2d");
	var chart3 = new Chart(chart3).Bar(chart3Data, {	responsive : true	} );
	    legend(document.getElementById("chart3Legend"), chart3Data);
}

function drawChart4(){
    var randomTemp = function() { return Math.floor(Math.random() * 20) + 65;}
	var chart4Data = {
		labels : ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"],
		datasets : [
			{
				label: "This Week",
				fillColor : "rgba(220,220,220,0.2)",
				strokeColor : "rgba(220,220,220,1)",
				pointColor : "rgba(220,220,220,1)",
				pointStrokeColor : "#fff",
				pointHighlightFill : "#fff",
				pointHighlightStroke : "rgba(220,220,220,1)",
				data : [randomTemp(),randomTemp(),randomTemp(),randomTemp(),randomTemp(),randomTemp(),randomTemp()]
			},
			{
				label: "Last Week",
				fillColor : "rgba(151,187,205,0.2)",
				strokeColor : "rgba(151,187,205,1)",
				pointColor : "rgba(151,187,205,1)",
				pointStrokeColor : "#fff",
				pointHighlightFill : "#fff",
				pointHighlightStroke : "rgba(151,187,205,1)",
				data : [randomTemp(),randomTemp(),randomTemp(),randomTemp(),randomTemp(),randomTemp(),randomTemp()]
			}
		]
	}
	var chart4 = document.getElementById("chart4").getContext("2d");
	var chart4 = new Chart(chart4).Line(chart4Data, { responsive: true	} );
	    legend(document.getElementById("chart4Legend"), chart4Data);
}

function drawLineCompareDayByHour() {

    var jsonData = [
    	{
    	'_id': '02394723749283',
    	'temp' : '74.5',
    	'light' : '540',
    	'millivolts' : '20',
    	'humidity' : '51'
        },
        {
    	'_id' : '12394723749283',
    	'temp' : '53.5',
    	'light' : '540',
    	'millivolts' : '20',
    	'humidity' : '51'        	
        }
        ];
	function getFields(input, field) {
	    var output = [];
	    for (var i=0; i < input.length ; ++i)
	        output.push(input[i][field]);
	    return output;
	}

	var temps = getFields(jsonData, 'temp');

	//var temps = <%= @current_user.get_last_day_temps_by_node(7) %>;
  	var randomTempGenerator = function() { return Math.floor(Math.random() * 20) + 65;}

	var chartLineCompareDayByHourData = {
		labels: ['12 AM', '1 AM', '2AM', '3AM', '4 AM',  '5 AM', '6 AM', '7 AM',  '8 AM', '9 AM',  '10 AM', '11 AM', '12 PM', '1 PM', '2PM', '3PM', '4 PM',  '5 PM', '6 PM', '7 PM',  '8 PM', '9 PM',  '10 PM', '11 PM'],
		datasets: [{
				label: "Today",
				fillColor: "rgba(157, 235, 136,0.4)",
				strokeColor: "#9deb88",
				pointColor: "#fff",
				pointStrokeColor: "#9deb88",
				data:  temps 
			}, {
				label: "Yesterday",
				fillColor: "rgba(137, 137, 235, 0.4)",
				strokeColor: "#8989eb",
				pointColor: "#fff",
				pointStrokeColor: "#8989eb",
				data: [randomTempGenerator(),randomTempGenerator(),randomTempGenerator(),randomTempGenerator(),randomTempGenerator(),randomTempGenerator(),
				       randomTempGenerator(),randomTempGenerator(),randomTempGenerator(),randomTempGenerator(),randomTempGenerator(),randomTempGenerator(),
				       randomTempGenerator(),randomTempGenerator(),randomTempGenerator(),randomTempGenerator(),randomTempGenerator(),randomTempGenerator(),
				       randomTempGenerator(),randomTempGenerator(),randomTempGenerator(),randomTempGenerator(),randomTempGenerator(),randomTempGenerator(),
				      ]
			}

		]
	}
	var chartLineCompareDayByHour = document.getElementById('chartLineCompareDayByHour').getContext('2d');
	var chartLineCompareDayByHour = new Chart(chartLineCompareDayByHour).Line(chartLineCompareDayByHourData, {
		responsive: true
	});
	
	    legend(document.getElementById("lineLegend"), chartLineCompareDayByHourData);

}

function drawBarCompareDayByHour() {
  var randomTempGenerator2 = function() { return Math.floor(Math.random() * 20) + 65;}
  var chartBarCompareDayByHourData = {
			labels: ['12 AM', '1 AM', '2AM', '3AM', '4 AM',  '5 AM', '6 AM', '7 AM',  '8 AM', '9 AM',  '10 AM', '11 AM',
			         '12 PM', '1 PM', '2PM', '3PM', '4 PM',  '5 PM', '6 PM', '7 PM',  '8 PM', '9 PM',  '10 PM', '11 PM'
			],
      datasets: [
          {
              label: 'Yesterday',
              fillColor: '#9deb88',
							data: [randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),
										 randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),
										 randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),
										 randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2()
			      ]
          },
          {
              label: 'Today',
              fillColor: '#8989eb',
							data: [randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),
							       randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),
							       randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),
							       randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2()
			      ]
          }
      ]
  };
  var chartBarCompareDayByHour = document.getElementById('chartBarCompareDayByHour').getContext('2d');
  var chartBarCompareDayByHour = new Chart(chartBarCompareDayByHour).Bar(chartBarCompareDayByHourData, 
  																		{	responsive : true
  																		//  , scaleOverride: true
  																		//  , scaleStartValue: '2 AM'
  																		//  , scaleSteps: null
									  									//  , animateScale : true
									  									//  , showTooltips: false
									  									//  , showScale: true
									  									//  , scaleShowLine : true
  																		});
 // document.getElementById('chartBarCompareDayByHour-legend').innerHTML = chartBarCompareDayByHour.generateLegend();
  legend(document.getElementById("barLegend"), chartBarCompareDayByHourData);

}

function drawRadar() {
    var randomTempGenerator2 = function() { return Math.floor(Math.random() * 20) + 65;}
	var radarData = {
	labels: ['12 AM', '1 AM', '2AM', '3AM', '4 AM',  '5 AM', '6 AM', '7 AM',  '8 AM', '9 AM',  '10 AM', '11 AM',
	         '12 PM', '1 PM', '2PM', '3PM', '4 PM',  '5 PM', '6 PM', '7 PM',  '8 PM', '9 PM',  '10 PM', '11 PM'
			],
    datasets: [
    	{
        label: "Today",
        fillColor: "rgba(157, 235, 136,0.3)",
        strokeColor: "rgba(157, 235, 136,0.5)",
        pointColor: "rgba(157, 235, 136,0.5)",
        pointStrokeColor: "rgba(157, 235, 136,0.5)",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "rgba(157, 235, 136,1)",
		data: [randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),
			   randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),
			   randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),
			   randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2()
			  ]	        
		},
    	{
        label: "Yesterday",
        fillColor: "rgba(137, 137, 235, 0.3)",
        strokeColor: "rgba(137, 137, 235, 0.5)",
        pointColor: "rgba(137, 137, 235, 0.5)",
        pointStrokeColor: "rgba(137, 137, 235, 0.5)",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "rgba(137, 137, 235,1)",
		data: [randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),
			   randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),
			   randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),
			   randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2(),randomTempGenerator2()
			  ]	 
		  }
	]
	};
	
	
	
    var radar = document.getElementById('radar').getContext('2d');
    var radar = new Chart(radar).Radar(radarData, {	responsive : true	});
    //document.getElementById('radar-legend').innerHTML = radar.generateLegend();
    legend(document.getElementById("radarLegend"), radarData);
	
}

function drawPolar() {
    var randomTemp = function() { return Math.floor(Math.random() * 20) + 65;}
	var polarData = [
	    {
	        value: randomTemp(),
	        color: "#F7464A",
	        highlight: "#FF5A5E",
	        label: "Today"
	    },
	    {
	        value: randomTemp(),
	        color: "#46BFBD",
	        highlight: "#5AD3D1",
	        label: "Yesterday"
	    }
	];
	
    var polar = document.getElementById('polar').getContext('2d');
    var polar = new Chart(polar).PolarArea(polarData, {	responsive : true , tooltipTemplate:"<%=label%>: <%=value%>g"	});
	  legend(document.getElementById("polarLegend"), polarData);

}



