$(function () {
  /**
 * Dark theme for Highcharts JS
 * @author Torstein Honsi
 */

// Load the fonts
Highcharts.createElement('link', {
  href: '//fonts.googleapis.com/css?family=Josefin+Sans',
  rel: 'stylesheet',
  type: 'text/css'
}, null, document.getElementsByTagName('head')[0]);

Highcharts.theme = {
  colors: ["#2b908f"
          , "#90ee7e"
          , "#f45b5b"
          , "#7798BF"
          , "#aaeeee"
          , "#ff0066"
          , "#eeaaee"
          , "#55BF3B"
          , "#DF5353"
          , "#7798BF"
          , "#aaeeee"
          ],
  chart: {
      backgroundColor: {
         linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
         stops: [
            [0, '#2a2a2b'],
            [1, '#3e3e40']
         ]
      },
      style: {
         fontFamily: "'Josefin Sans', sans-serif"
      },
      plotBorderColor: '#606063'
  },
  title: {
      style: {
         color: '#E0E0E3',
         textTransform: 'uppercase',
         fontSize: '20px'
      }
  },
  subtitle: {
      style: {
         color: '#E0E0E3',
         textTransform: 'uppercase'
      }
  },
  xAxis: {
      gridLineColor: '#707073',
      labels: {
         style: {
            color: '#E0E0E3'
         }
      },
      lineColor: '#707073',
      minorGridLineColor: '#505053',
      tickColor: '#707073',
      title: {
         style: {
            color: '#A0A0A3'

         }
      }
  },
  yAxis: {
      gridLineColor: '#707073',
      labels: {
         style: {
            color: '#E0E0E3'
         }
      },
      lineColor: '#707073',
      minorGridLineColor: '#505053',
      tickColor: '#707073',
      tickWidth: 1,
      title: {
         style: {
            color: '#A0A0A3'
         }
      }
  },
  tooltip: {
      backgroundColor: 'rgba(0, 0, 0, 0.85)',
      style: {
         color: '#F0F0F0'
      }
  },
  plotOptions: {
      series: {
         dataLabels: {
            color: '#B0B0B3'
         },
         marker: {
            lineColor: '#333'
         }
      },
      boxplot: {
         fillColor: '#505053'
      },
      candlestick: {
         lineColor: 'white'
      },
      errorbar: {
         color: 'white'
      }
  },
  legend: {
      itemStyle: {
         color: '#E0E0E3'
      },
      itemHoverStyle: {
         color: '#FFF'
      },
      itemHiddenStyle: {
         color: '#606063'
      }
  },
  credits: {
      style: {
         color: '#666'
      }
  },
  labels: {
      style: {
         color: '#707073'
      }
  },

  drilldown: {
      activeAxisLabelStyle: {
         color: '#F0F0F3'
      },
      activeDataLabelStyle: {
         color: '#F0F0F3'
      }
  },

  navigation: {
      buttonOptions: {
         symbolStroke: '#DDDDDD',
         theme: {
            fill: '#505053'
         }
      }
  },

  // scroll charts
  rangeSelector: {
      buttonTheme: {
         fill: '#505053',
         stroke: '#000000',
         style: {
            color: '#CCC'
         },
         states: {
            hover: {
              fill: '#707073',
              stroke: '#000000',
              style: {
                  color: 'white'
              }
            },
            select: {
              fill: '#000003',
              stroke: '#000000',
              style: {
                  color: 'white'
              }
            }
         }
      },
      inputBoxBorderColor: '#505053',
      inputStyle: {
         backgroundColor: '#333',
         color: 'silver'
      },
      labelStyle: {
         color: 'silver'
      }
  },

  navigator: {
      handles: {
         backgroundColor: '#666',
         borderColor: '#AAA'
      },
      outlineColor: '#CCC',
      maskFill: 'rgba(255,255,255,0.1)',
      series: {
         color: '#7798BF',
         lineColor: '#A6C7ED'
      },
      xAxis: {
         gridLineColor: '#505053'
      }
  },

  scrollbar: {
      barBackgroundColor: '#808083',
      barBorderColor: '#808083',
      buttonArrowColor: '#CCC',
      buttonBackgroundColor: '#606063',
      buttonBorderColor: '#606063',
      rifleColor: '#FFF',
      trackBackgroundColor: '#404043',
      trackBorderColor: '#404043'
  },

  // special colors for some of the
  legendBackgroundColor: 'rgba(0, 0, 0, 0.5)',
  background2: '#505053',
  dataLabelsColor: '#B0B0B3',
  textColor: '#C0C0C0',
  contrastTextColor: '#F0F0F3',
  maskColor: 'rgba(255,255,255,0.3)'
};

// Apply the theme
Highcharts.setOptions(Highcharts.theme); 
});


$(function () {
    $('#heatMap').highcharts({

        chart: {
            type: 'heatmap',
            marginTop: 40,
            marginBottom: 80,
            plotBorderWidth: 1
        },


        title: {
            text: 'Readings per room'
        },

        xAxis: {
            categories: ['Room 5', 'Room 7']
        },

        yAxis: {
            categories: ['Heat', 'Temperature', 'Light'],
            title: null
        },

        colorAxis: {
            min: 0,
            minColor: '#FFFFFF',
            maxColor: Highcharts.getOptions().colors[0]
        },

        legend: {
            align: 'right',
            layout: 'vertical',
            margin: 0,
            verticalAlign: 'top',
            y: 25,
            symbolHeight: 280
        },

        tooltip: {
            formatter: function () {
                return '<b>' + this.series.xAxis.categories[this.point.x] + '</b> sold <br><b>' +
                    this.point.value + '</b> items on <br><b>' + this.series.yAxis.categories[this.point.y] + '</b>';
            }
        },

        series: [{
            name: 'Sales per employee',
            borderWidth: 1,
            data: [ [0, 2, 123], [8, 3, 64], [8, 4, 84], [9, 0, 47], [9, 1, 114], [9, 2, 31], [9, 3, 48], [9, 4, 91]],
            dataLabels: {
                enabled: true,
                color: '#000000'
            }
        }]

    });
});



$(function () {
    $('#highChart').highcharts({
       
        title: {
            text: 'Monthly Average Temperature',
            x: -20 //center
        },
        subtitle: {
            text: 'Source: WorldClimate.com',
            x: -20
        },
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
        yAxis: {
            title: {
                text: 'Temperature (°F)'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: '°F'
        },
        legend: {
            layout: 'horizontal',
            align: 'left',
            verticalAlign: 'bottom',
            borderWidth: 0
        },
        series: [{
            name: 'Weekdays',
            data: [62, 62, 63, 65, 68, 73, 77, 76, 74, 70, 67, 65]
        }, {
            name: 'Weeknights',
            data: [67, 67, 67, 68, 68, 68, 68, 68, 68, 68, 67, 67]
        }, {
            name: 'Weekend Days',
            data: [67, 67, 67, 68, 68, 69, 69, 68, 68, 68, 67, 67]
        }, {
            name: 'Weekend Nights',
            data: [62, 63, 64, 64, 64, 63, 63, 63, 63, 62, 62, 62]
        }]
    });
});

$(function () {
    // Create the chart
    $('#fireFloodDiv').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Room Temperatures'
        },
        subtitle: {
            text: 'Current Temperatures Within Your Home'
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Temperature'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f} °F'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f} °F</b><br/>'
        },

        series: [{
            name: 'Rooms',
            colorByPoint: true,
            data: [{
                name: 'Room1',
                y: 56.33,
                drilldown: 'Room1'
            }, {
                name: 'Room2',
                y: 64.03,
                drilldown: 'Room2'
            }, {
                name: 'Room3',
                y: 75.38,
                drilldown: 'Room3'
            }, {
                name: 'Room4',
                y: 64.77,
                drilldown: 'Room4'
            }, {
                name: 'Room5',
                y: 60.91,
                drilldown: 'Room5'
            }, {
                name: 'Room6',
                y: 60.2,
                drilldown: null
            }]
        }],
        drilldown: {
            series: [{
                name: 'Room1',
                id: 'Room1',
                data: [
                    [
                        'Temperature Now',
                        56.33
                    ],
                    [
                        '15 Minutes Ago',
                        58.33
                    ],
                    [
                        '30 Minutes Ago',
                        60
                    ],
                    [
                        '1 Hour Ago',
                        65
                    ],
                    [
                        '1.5 Hours Ago',
                        68
                    ],
                    [
                        '2 Hours Ago',
                        69
                    ]
                ]
            }, {
                name: 'Room2',
                id: 'Room2',
                data: [
                    [
                        'Temperature Now',
                        64.03
                    ],
                    [
                        '15 Minutes Ago',
                        68
                    ],
                    [
                        '30 Minutes Ago',
                        68.11
                    ],
                    [
                        '1 Hour Ago',
                        69.33
                    ],
                    [
                        '1.5 Hours Ago',
                        71.06
                    ],
                    [
                        '2 Hours Ago',
                        70.5
                    ]
                ]
            }, {
                name: 'Room3',
                id: 'Room3',
                data: [
                    [
                        'Temperature Now',
                        75.38
                    ],
                    [
                        '15 Minutes Ago',
                        72.2
                    ],
                    [
                        '30 Minutes Ago',
                        71.11
                    ],
                    [
                        '1 Hour Ago',
                        73.33
                    ],
                    [
                        '1.5 Hours Ago',
                        74.06
                    ],
                    [
                        '2 Hours Ago',
                        75.5
                    ]
                ]
            }, {
                name: 'Room4',
                id: 'Room4',
                data: [
                    [
                        'Temperature Now',
                        64.77
                    ],
                    [
                        '15 Minutes Ago',
                        67.2
                    ],
                    [
                        '30 Minutes Ago',
                        68.11
                    ],
                    [
                        '1 Hour Ago',
                        65.33
                    ],
                    [
                        '1.5 Hours Ago',
                        71.06
                    ],
                    [
                        '2 Hours Ago',
                        70.5
                    ]
                ]
            }, {
                name: 'Room5',
                id: 'Room5',
                data: [
                    [
                        'Temperature Now',
                      60.91
                    ],
                    [
                        '15 Minutes Ago',
                        63.2
                    ],
                    [
                        '30 Minutes Ago',
                        65.11
                    ],
                    [
                        '1 Hour Ago',
                        65.33
                    ],
                    [
                        '1.5 Hours Ago',
                        67.06
                    ],
                    [
                        '2 Hours Ago',
                        70.5
                    ]
                ]
            }]
        }
    });
});


$(function () {
    $('#TempVsHumidity').highcharts({
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: 'Average Monthly Temperature and Humidity'
        },
        subtitle: {
            text: 'Whole House'
        },
        xAxis: [{
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            crosshair: true
        }],
        yAxis: [{ // Primary yAxis
            labels: {
                format: '{value}°F',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            title: {
                text: 'Temperature',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            }
        }, { // Secondary yAxis
            title: {
                text: 'Humidity',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            labels: {
                format: '{value} %',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            opposite: true
        }],
        tooltip: {
            shared: true
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            x: 120,
            verticalAlign: 'top',
            y: 100,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        series: [{
            name: 'Humidity',
            type: 'column',
            yAxis: 1,
            data: [19.9, 20.5, 30.4, 58.2, 72.0, 47.0, 60.6, 42.5, 45.4, 70.1, 20.6, 24.4],
            tooltip: {
                valueSuffix: ' %'
            }

        }, {
            name: 'Temperature',
            type: 'spline',
            data: [67.0, 66.9, 69.5, 74.5, 78.2, 71.5, 75.2, 76.5, 73.3, 68.3, 63.9, 62.6],
            tooltip: {
                valueSuffix: '°F'
            }
        }]
    });
});


$(function () {
    $(document).ready(function () {
      Highcharts.setOptions({
            global: {
                useUTC: false
            }
      });

        $('#livedata').highcharts({
            chart: {
                type: 'spline',
                animation: Highcharts.svg, // don't animate in old IE
                marginRight: 10,
                events: {
                    load: function () {

                        // set up the updating of the chart each second
                        var series1 = this.series[0];
                        setInterval(function () {
                            var x = (new Date()).getTime(), // current time
                                y = (Math.random() * 20) + 65;
                            series1.addPoint([x, y], true, true);
                        }, 5000);
                        
                        var series2 = this.series[1];
                        setInterval(function () {
                            var x = (new Date()).getTime(), // current time
                                y = (Math.random() * 20) + 65;
                            series2.addPoint([x, y], true, true);
                        }, 5000);
                        
                      var series3 = this.series[2];
                        setInterval(function () {
                            var x = (new Date()).getTime(), // current time
                                y = (Math.random() * 20) + 65;
                            series3.addPoint([x, y], true, true);
                        }, 3000); 
                        

                        
                    }
                }
            },
            title: {
                text: 'Live temperature data'
            },
            xAxis: {
                type: 'datetime',
                tickPixelInterval: 150
            },
            yAxis: {
                title: {
                    text: 'Value'
                },
                plotLines: [{
                    value: 10,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + '</b><br/>' +
                        Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
                        Highcharts.numberFormat(this.y, 2);
                }
            },
            legend: {
                enabled: true
            },
            exporting: {
                enabled: false
            },
            series: [
              {
                name: 'Room1',
                data: (function () {
                    // generate an array of random data
                    var data1 = [],
                        time = (new Date()).getTime(),
                        i;

                    for (i = -19; i <= 0; i += 1) {
                        data1.push({
                            x: time + i * 3000,
                            y: (Math.random() * 20) + 65,
                        });
                    }
                    return data1;
                }())
            },{
                name: 'Room2',
                data: (function () {
                    // generate an array of random data
                    var data2 = [],
                        time = (new Date()).getTime(),
                        i;

                    for (i = -19; i <= 0; i += 1) {
                        data2.push({
                            x: time + i * 3000,
                            y: (Math.random() * 20) + 65,
                        });
                    }
                    return data2;
                }())
            },{
                name: 'Room3',
                data: (function () {
                    // generate an array of random data
                    var data3 = [],
                        time = (new Date()).getTime(),
                        i;

                    for (i = -19; i <= 0; i += 1) {
                        data3.push({
                            x: time + i * 3000,
                            y: (Math.random() * 20) + 65,
                        });
                    }
                    return data3;
                }())
            }
            ]
        });
    });
});


$(function () {
    $('#TempVsHumidity2').highcharts({
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: 'Average Monthly Temperature and Humidity'
        },
        subtitle: {
            text: 'Room 1'
        },
        xAxis: [{
            categories: ['12 AM', '1 AM', '2AM', '3AM', '4 AM',  '5 AM', '6 AM', '7 AM',  '8 AM', '9 AM',  '10 AM', '11 AM',
	                      '12 PM', '1 PM', '2PM', '3PM', '4 PM',  '5 PM', '6 PM', '7 PM',  '8 PM', '9 PM',  '10 PM', '11 PM'],
            crosshair: true
        }],
        yAxis: [{ // Primary yAxis
            labels: {
                format: '{value}°F',
              //  style: {
              //     color: Highcharts.getOptions().colors[3]
              //  }
            },
            title: {
                text: 'Temperature',
              //  style: {
              //     color: Highcharts.getOptions().colors[3]
              //  }
            }
        }, { // Secondary yAxis
            title: {
                text: 'Humidity',
              //  style: {
              //     color: Highcharts.getOptions().colors[3]
              //  }
            },
            labels: {
                format: '{value} %',
              //  style: {
              //     color: Highcharts.getOptions().colors[3]
              //  }
            },
            opposite: true
        }],
        tooltip: {
            shared: false
        },
        legend: {
            layout: 'horizontal',
            align: 'left',
            width: 300,
            x: 0,
            verticalAlign: 'bottom',
            y: 0,
            floating: false,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        series: [{
            name: 'Humidity Today',
            type: 'column',
            color: Highcharts.getOptions().colors[0],
            yAxis: 1,
            data: [19.9, 20.5, 30.4, 58.2, 72.0, 47.0, 60.6, 42.5, 45.4, 70.1, 20.6, 24.4, 19.9, 20.5, 30.4, 58.2, 72.0, 47.0, 60.6, 42.5, 45.4, 70.1, 20.6, 24.4],
            tooltip: {
                valueSuffix: ' %'
            }
            
        },{
            name: 'Humidity Yesterday',
            type: 'column',
            color: Highcharts.getOptions().colors[1],

            yAxis: 1,
            data: [60.6, 42.5, 45.4, 70.1, 20.6, 24.4, 19.9, 20.5, 30.4, 58.2, 72.0, 47.0, 60.6, 42.5, 45.4, 70.1, 20.6, 24.4, 19.9, 20.5, 30.4, 58.2, 72.0, 47.0],
            tooltip: {
                valueSuffix: ' %'
            }

        }, {
            name: 'Temperature Today',
            type: 'spline',
            color: Highcharts.getOptions().colors[0],
            data: [67.0, 66.9, 69.5, 74.5, 78.2, 71.5, 75.2, 76.5, 73.3, 68.3, 63.9, 62.6, 67.0, 66.9, 69.5, 74.5, 78.2, 71.5, 75.2, 76.5, 73.3, 68.3, 63.9, 62.6],
            tooltip: {
                valueSuffix: '°F'
            }
        }, {
            name: 'Temperature Yesterday',
            type: 'spline',
            color: Highcharts.getOptions().colors[1],

            data: [75.2, 76.5, 73.3, 68.3, 63.9, 62.6, 66.9, 69.5, 74.5, 78.2, 76.5, 73.3, 75.2, 76.5, 73.3, 68.3, 63.9, 62.6, 66.9, 69.5, 74.5, 78.2, 76.5, 73.3],
            tooltip: {
                valueSuffix: '°F'
            }
        }]
    });
});

        
$(function () {

    $('#all3').highcharts({
        
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: 'Average Monthly Data'
        },
        subtitle: {
            text: ''
        },
        xAxis: [{
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            crosshair: true
        }],
        yAxis: [{ // Primary yAxis
            labels: {
                format: '{value}°F',
                style: {
                    color: Highcharts.getOptions().colors[2]
                }
            },
            title: {
                text: 'Temperature',
                style: {
                    color: Highcharts.getOptions().colors[2]
                }
            },
            opposite: true

        }, { // Secondary yAxis
            gridLineWidth: 0,
            title: {
                text: 'Humidity',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            labels: {
                format: '{value} %',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            }

        }, { // Tertiary yAxis
            gridLineWidth: 0,
            title: {
                text: 'Light levels',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            labels: {
                format: '{value} lm',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            opposite: true
        }],
        tooltip: {
            shared: true
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            x: 80,
            verticalAlign: 'top',
            y: 55,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        series: [{
            name: 'Humidity',
            type: 'column',
            yAxis: 1,
            data: temps ,
            tooltip: {
                valueSuffix: ' mm'
            }

        }, {
            name: 'Light',
            type: 'spline',
            yAxis: 2,
            data: [30, 45, 56.9, 62.5, 72.3, 77.5, 80.6, 90.2, 80.1, 85.9, 70.2, 30.7],
            marker: {
                enabled: false
            },
            dashStyle: 'shortdot',
            tooltip: {
                valueSuffix: ' mb'
            }

        }, {
            name: 'Temperature',
            type: 'spline',
            data: [67.0, 65.9, 68.5, 70.5, 73.2, 75.5, 78.2, 72.5, 79.3, 65.3, 67.9, 68.6],
            tooltip: {
                valueSuffix: ' °C'
            }
        }]
    });
    
});

$(function () {
    $('#treemap').highcharts({
        colorAxis: {
            minColor: Highcharts.getOptions().colors[3],
            maxColor: Highcharts.getOptions().colors[8]
        },
        series: [{
            type: 'treemap',
            layoutAlgorithm: 'squarified',
            //layoutAlgorithm: 'stripes',
            //layoutAlgorithm: 'strip',

            data: [{
                name: 'Room1',
                value: 72.4,
                colorValue: 4
            }, {
                name: 'Room 2',
                value: 77.3,
                colorValue: 5
            }, {
                name: 'Room 3',
                value: 66.2,
                colorValue: 2
            }, {
                name: 'Room 5',
                value: 69.2,
                colorValue: 3
            }, {
                name: 'Room 6',
                value: 58.6,
                colorValue: 1
            }]
        }],
        title: {
            text: 'Highcharts Treemap'
        }
    });
});

